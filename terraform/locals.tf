locals {
  name = "fueled-cluster"
  # var.cluster_name is for Terratest
  cluster_name = coalesce(var.cluster_name, local.name)
  region       = "us-east-1"

  vpc_cidr = "10.0.0.0/16"
  azs      = slice(data.aws_availability_zones.available.names, 0, 3)

  tags = {
    Name        = local.name
    Environment = "${terraform.workspace}"
    Project     = "fueled-platforms-assignment-1.0.0"
  }
}