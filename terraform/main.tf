provider "aws" {
  region = local.region
}

provider "kubernetes" {
  host                   = module.fueled_eks.eks_cluster_endpoint
  cluster_ca_certificate = base64decode(module.fueled_eks.eks_cluster_certificate_authority_data)
  token                  = data.aws_eks_cluster_auth.this.token
}

provider "helm" {
  kubernetes {
    host                   = module.fueled_eks.eks_cluster_endpoint
    cluster_ca_certificate = base64decode(module.fueled_eks.eks_cluster_certificate_authority_data)
    token                  = data.aws_eks_cluster_auth.this.token
  }
}

data "aws_eks_cluster_auth" "this" {
  name = module.fueled_eks.eks_cluster_id
}

data "aws_availability_zones" "available" {
  filter {
    name   = "opt-in-status"
    values = ["opt-in-not-required"]
  }
}

#---------------------------------------------------------------
# EKS
#---------------------------------------------------------------

module "fueled_eks" {
  source = "github.com/aws-ia/terraform-aws-eks-blueprints?ref=v4.20.0"

  cluster_name    = local.cluster_name
  cluster_version = "1.24"

  vpc_id             = module.vpc.vpc_id
  private_subnet_ids = module.vpc.private_subnets

  node_security_group_additional_rules = {
    # Extend node-to-node security group rules. Recommended and required for the Add-ons
    ingress_self_all = {
      description = "Node to node all ports/protocols"
      protocol    = "-1"
      from_port   = 0
      to_port     = 0
      type        = "ingress"
      self        = true
    }
    # Recommended outbound traffic for Node groups
    egress_all = {
      description      = "Node all egress"
      protocol         = "-1"
      from_port        = 0
      to_port          = 0
      type             = "egress"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
    # Allows Control Plane Nodes to talk to Worker nodes on all ports. Added this to simplify the example and further avoid issues with Add-ons communication with Control plane.
    # This can be restricted further to specific port based on the requirement for each Add-on e.g., metrics-server 4443, spark-operator 8080, karpenter 8443 etc.
    # Change this according to your security requirements if needed
    ingress_cluster_to_node_all_traffic = {
      description                   = "Cluster API to Nodegroup all traffic"
      protocol                      = "-1"
      from_port                     = 0
      to_port                       = 0
      type                          = "ingress"
      source_cluster_security_group = true
    }
    # inbound rules for port 8833, 8844, 9933, 9944
    ingress_cluster_to_node_8833_8844 = {
      description                   = "Cluster API to Nodegroup 8833, 8844, 9933, 9944"
      protocol                      = "tcp"
      from_port                     = 8833
      to_port                       = 8844
      type                          = "ingress"
      source_cluster_security_group = true
    }

    ingress_cluster_to_node_9933_9944 = {
      description                   = "Cluster API to Nodegroup 9933, 9944"
      protocol                      = "tcp"
      from_port                     = 9933
      to_port                       = 9944
      type                          = "ingress"
      source_cluster_security_group = true
    }
  }

  managed_node_groups = {
    t3 = {
      node_group_name = "managed-ondemand"
      instance_types  = ["t3.medium"]
      min_size        = 3
      max_size        = 5
      desired_size    = 3
      subnet_ids      = module.vpc.private_subnets
    }
  }

  tags = local.tags
}

#---------------------------------------------------------------
# Supporting Resources
#---------------------------------------------------------------

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.0"

  name = local.name
  cidr = local.vpc_cidr

  azs             = local.azs
  public_subnets  = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k)]
  private_subnets = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k + 10)]

  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  # Manage so we can name
  manage_default_network_acl    = true
  default_network_acl_tags      = { Name = "${local.name}-default-nacl" }
  manage_default_route_table    = true
  default_route_table_tags      = { Name = "${local.name}-default-rtb" }
  manage_default_security_group = true
  default_security_group_tags   = { Name = "${local.name}-default-sg" }

  private_route_table_tags = { Name = "${local.name}-private-rtb" }
  public_route_table_tags  = { Name = "${local.name}-public-rtb" }
  nat_gateway_tags         = { Name = "${local.name}-nat-gw" }

  public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = 1
    "Name"                                        = "${local.name}-publicsubnet"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"             = 1
    "Name"                                        = "${local.name}-privatesubnet"
  }

  tags = local.tags
}

#----------------------------------------------------------------------------------------------------------------
# Create ECR Repository
#----------------------------------------------------------------------------------------------------------------

# Create Frontend Repository
# resource "aws_ecr_repository" "frontend_repository" {
#   name                 = "webfrontend"
#   image_tag_mutability = "MUTABLE"

#   image_scanning_configuration {
#     scan_on_push = true
#   }
# }

# # Create Backend Repository
# resource "aws_ecr_repository" "backend_repository" {
#   name                 = "webserver"
#   image_tag_mutability = "MUTABLE"

#   image_scanning_configuration {
#     scan_on_push = true
#   }
# }

#----------------------------------------------------------------------------------------------------------------
# Certificate Creation and Validation
#----------------------------------------------------------------------------------------------------------------

# data "aws_route53_zone" "domain" {
#   name = var.domain_name
# }

# resource "aws_route53_record" "cert_validation" {
#   for_each = {
#     for d in aws_acm_certificate.cert.domain_validation_options : d.domain_name => {
#       name   = d.resource_record_name
#       record = d.resource_record_value
#       type   = d.resource_record_type
#     }
#   }

#   allow_overwrite = true
#   name            = each.value.name
#   records         = [each.value.record]
#   ttl             = 60
#   type            = each.value.type
#   zone_id         = data.aws_route53_zone.domain.zone_id
# }


# resource "aws_acm_certificate" "cert" {
#   domain_name               = var.domain_name
#   subject_alternative_names = ["*.${var.domain_name}"]
#   validation_method         = "DNS"
#   tags                      = local.tags
# }

# resource "aws_acm_certificate_validation" "cert" {
#   certificate_arn         = aws_acm_certificate.cert.arn
#   validation_record_fqdns = [for r in aws_route53_record.cert_validation : r.fqdn]
# }