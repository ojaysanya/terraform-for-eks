output "cluster_id" {
  description = "EKS cluster ID."
  value       = module.fueled_eks.eks_cluster_id
}
output "cluster_endpoint" {
  description = "Endpoint for EKS control plane."
  value       = module.fueled_eks.eks_cluster_endpoint
}

output "cluster_security_group_id" {
  description = "Security group ids attached to the cluster control plane."
  value       = module.fueled_eks.cluster_security_group_id
}

output "region" {
  description = "AWS region"
  value       = "us-east-1"
}

output "cluster_name" {
  description = "Kubernetes Cluster Name"
  value       = local.cluster_name
}

output "configure_kubectl" {
  description = "Configure kubectl: make sure you're logged in with the correct AWS profile and run the following command to update your kubeconfig"
  value       = module.fueled_eks.configure_kubectl
}

# output "frontend_repository" {
#   description = "The repository url for frontend app"
#   value       = aws_ecr_repository.frontend_repository.repository_url
# }

# output "backend_repository" {
#   description = "The repository url for backend app"
#   value       = aws_ecr_repository.backend_repository.repository_url
# }

# output "frontend_certificate" {
#   description = "The certificate arn which the frontend app will use"
#   value = aws_acm_certificate.cert.arn
# }