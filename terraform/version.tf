terraform {
  required_version = ">= 1.0.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.72"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.10"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.4.1"
    }
  }
}

#   # Configure s3 backend for terraform state and Dynamo DB for state locking
#   backend "s3" {
#     bucket         = "fueled-assignment-state"
#     key            = "terraform/terraform.tfstate"
#     region         = "us-east-1"
#     dynamodb_table = "fueled-assignment-state-lock"
#     encrypt        = true
#   }
# }