variable "cluster_name" {
  description = "Name of cluster - used by Terratest for e2e test automation"
  type        = string
  default     = ""
}

variable "domain_name" {
  description = "Domain name for the fueled frontend app"
  type = string
  default = "techsage.tk"
}