# Solution

## Introduction

- The solution provides a highly available, scalable, and secure infrastructure for the frontend and backend applications
- The solution also provides observability into the applications' performance and behaviour
- Please refer to the README and documentation within the codebase for more information and instructions on how to run the project.

## Architecture

This is the high-level architecture diagram for the deployment:

![fueled-high-level-architecture](fueled-high-level-architecture.png)

- The architecture consists of a VPC with 3 public subnets and 3 private subnets. Traffic passing to the internet from the Private subnets goes through the NAT gateway attached to the private subnets by defining routes through a private route table. This is a security feature to ensure that the workloads in the private subnet are secure.

- The EKS cluster is deployed into the private subnets with strict security group rules to control access to the nodes. It has a single node group which has a minimum of 3 nodes in the 3 availability zones for high availability, to ensure business continuity in the event of failure of any of the nodes. The node instance type is general purpose t3.medium instance size.

- The system uses Amazon Certificate Manager (ACM) to provide the SSL/TLS certificate for the security and encryption of the domain. Also, a public route53 zone is used for enabling custom domain routing to the frontend app.

## Infrastructure Provisioning

- The infrastructure is provisioned and configured on AWS using Terraform
- VPC, subnets, security groups and an EKS cluster are provisioned
- The terraform code can be found in the `terraform` directory of the project. s3 is configured as the Terraform backend and DynamoDB is used for state locking. 
- Here I opted for the external terraform modules hosted on GitHub repositories and locked with their respective versions to prevent breaking changes from upgrades. It is a lot easier than writing out all the modules from scratch
- Kubernetes IAM Roles for Service Accounts (IRSA) are used to grant access to certain AWS services like ECR and Load Balancers also as part of a secure mode of access.
- To deploy the infrastructure to multiple environments, we use terraform workspaces. Before running the terraform commands for init, plan and deploy, we create workspaces for dev, staging and prod. With workspaces, you have a logical separation of your terraform state files in the remote backend without making any changes to the terraform code itself.

```bash
terraform workspace new dev # To create a new workspace for development
terraform workspace new staging # To create a new workspace for staging
terraform workspace new prod # To create a new workspace for prod
```

- After selecting the workspace, When in the terraform directory, to deploy the infrastructure, you can run to deploy the resources:

```bash
terraform init # To initialize the providers and modules
terraform plan # To validate the deployment
terraform apply # To deploy the resources
```

## Containerization

- The frontend and backend applications are containerized using Docker
- The images are then tagged and pushed to a remote docker repository - ECR which is already created using terraform code
- The images are also scanned for any vulnerabilities in the ECR repository as part of the security measures
- To build the image for the frontend application. change into the `frontend` directory of the project and run:

```bash
docker build --platform=linux/amd64 -t 727906470930.dkr.ecr.us-east-1.amazonaws.com/webfrontend:1.16.0 . # To build and tag the image

docker push 727906470930.dkr.ecr.us-east-1.amazonaws.com/webfrontend:1.16.0 # To push the image to the remote repository
```

- To build the image for the frontend application. change into the `backend` directory of the project and run:

```bash
docker build --platform=linux/amd64 -t 727906470930.dkr.ecr.us-east-1.amazonaws.com/webserver:1.16.0 . # To build and tag the image

docker push 727906470930.dkr.ecr.us-east-1.amazonaws.com/webserver:1.16.0 # To push the image to the remote repository
```

## Application Deployment

- Kubernetes is used to deploy and manage the frontend and backend applications
- Frontend application is deployed as public service, exposed using Ingress and the ALB Load Balancer Controller.
- Backend service is deployed as a private service accessible only within the EKS cluster in the VPC
- Both services are deployed as Kubernetes pods and managed by a Kubernetes Deployment
- Helm charts are used to package and deploy the applications to make it easier to manage and roll back releases if required
- The Kubernetes deployment files can be found in the `k8s-deployment/charts` directory
- A namespace `apps` is used just to separate the apps into a logical unit.
- The pods have a minimum replica of 2 for availability and load-balancing
- Then liveness and readiness probes are also configured in line with best practices.
- Resource Requests and Limits are also set in line with best practices for deploying applications.
- Kubernetes metrics server was deployed as well to get basic monitoring information about the cluster set-up and most importantly to track resource utilization in the set-up of the Horizontal Pod Autoscaler (HPA) for the apps to automatically scale.
- To deploy to multiple environments using helm, we create multiple values.yaml file per environment. So that we can change the environment-specific settings. For example, `dev-values.yaml`, `staging-values. yaml`, and `prod-values.yaml`. Then pass an additional `--values` flag with the `helm install` command.

- To deploy the backend application, while in the k8s-deployment/charts, run:

```bash
helm install backend-service backend-chart -n apps
```

- To deploy the frontend application, while in the k8s-deployment/charts, run:

```bash
helm install fueled-frontend frontend-chart -n apps 
```

- To deploy to multiple environments using helm, we create multiple values.yaml file per environment. So that we can change the environment-specific settings. For example, `dev-values.yaml`, `staging-values.yaml`, and `prod-values.yaml`. Then pass an additional `--values` flag with the `helm install` command.

- See the final Application Output hosted on the EKS Cluster.

![app-deployed](app-deployed.png)

## Security, Reliability and Observability

- The solution is designed with security, reliability, and observability in mind
- The Kubernetes cluster is configured to automatically scale the applications based on usage
- Metrics and logs emitted by the application are collected and monitored using Prometheus and Grafana
- Prometheus and Grafana are set up using the official [kube-prometheus-stack](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack) by Prometheus-community to monitor the EKS cluster and the application endpoints:

```bash
# Get Helm Repository Info
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update

# Install Helm Chart
helm install fueled-monitor prometheus-community/kube-prometheus-stack
```
